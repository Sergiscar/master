<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Support\Facades\Storage;
use Illuminate\Queue\SerializesModels;

class newsletterMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $emails;
    public function __construct($emails)
    {
        $this->emails = $emails;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $url=  Storage::url((json_decode($this->emails['file']))[0]->download_link);
      //  dd($url);
        return $this->from($address = 'contact@anama.bj', $name = 'SoNaMA')
                    ->subject('Newsletter')
                    ->view('newslettermail')
                    ->with(['emails', $this->emails])->attach(public_path($url), [
                        'as' => 'Newsletter.pdf',
                        'mime' => 'application/pdf',
                   ]);;
    }
}
