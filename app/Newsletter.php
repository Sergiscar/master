<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;


class Newsletter extends Model
{
    use HasFactory;
    protected $fillable = ['titre','image','file','parution','courte_description'];
}
