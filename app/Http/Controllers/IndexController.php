<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Subscriber;
use Illuminate\Support\Facades\Mail;
use App\Mail\sendingEmail;

class IndexController extends Controller
{
    //
    public function index()
    {
         // carrousels
         $ingerierie = DB::table('services')->where('proposition', '=', "SIRCAH")->first();
         $production = DB::table('services')->where('proposition', '=', "SPEAMGCM")->first();
         $numerique = DB::table('services')->where('proposition', '=', "SSI")->first();
         $commercial = DB::table('services')->where('proposition', '=', "SCM")->first();
         $sav = DB::table('services')->where('proposition', '=', "SAV")->first();
         $carrousels = DB::table('carrousels')->whereIn('id', [3, 2])->get();
         $actualites  =  DB::table('actualites')->limit(4)->orderBy('actualites.created_at','DESC')->get();
         $services  =  DB::table('services')->limit(6)->orderBy('services.id','ASC')->get();
         $partenaires  =  DB::table('partenaires')->limit(6)->orderBy('partenaires.id','ASC')->get();
         $description = DB::table('sitedatas')->first(); 
         $cles = DB::table('categorieactuelites')->get();
         $post = DB::table('descriptionsonamas')->first();
        return view('home',compact('ingerierie','production','numerique','commercial','sav','post','carrousels','actualites','services','partenaires','description','cles'));
    }
    public function store(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|unique:subscribers,email',
        ]);
        $input = $request->all();
        $program = Subscriber::create($input);
        $data = array(
            'message' => "Vous avez souscrit à notre newsletter avec succès. Vous recevrez par mail nos prochaines parutions !"
        );
       
        Mail::to($request->input('email'))->send(new sendingEmail($data));
        return redirect()->route('Accueil')->with('message','Vous avez souscrit à notre newsletter avec succès. Vous recevrez par mail nos prochaines parutions !');
    }
    

}
