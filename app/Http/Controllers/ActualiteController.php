<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;
use App\Newsletter;
use App\Subscriber;
use Illuminate\Support\Facades\Mail;
use App\Mail\newsletterMail;
use Carbon;

class ActualiteController extends Controller
{
    public function index()
    {
         $carrousels = DB::table('carrousels')->whereIn('id', [4,5,6])->get();
         $description = DB::table('sitedatas')->first(); 
         $cles = DB::table('categorieactuelites')->get();
         $actualites = DB::table('actualites')->orderBy('actualites.created_at','DESC')->paginate(6);
         return view('actualite',compact('actualites','description','cles','carrousels'));
    }
    public function societe()
    {
         $carrousels = DB::table('carrousels')->whereIn('id', [4,5,7])->get();
         $description = DB::table('sitedatas')->first(); 
         $cles = DB::table('categorieactuelites')->get();
         $post = DB::table('descriptionsonamas')->first();
         return view('societe',compact('description','cles','carrousels','post'));
    }

    public function actualitedetail($id)
    {
       
          $item = DB::table('actualites')->where('id', '=', $id)->first();
        $description = DB::table('sitedatas')->first(); 
         $cles = DB::table('categorieactuelites')->get();
         $carrousels = DB::table('carrousels')->orderBy('carrousels.id','DESC')->limit(3)->get();
        
        return view('actualitedetail',compact('item','description','cles','carrousels'));
    }

    public function indexadministration()
    {   
        $description = DB::table('sitedatas')->first(); 
        $cles = DB::table('categorieactuelites')->get();
        $carrousels = DB::table('carrousels')->whereIn('id', [6])->get();
        $administration = DB::table('personnels')->whereNotIn('nom',[''])->orderBy('ordre','ASC')->paginate(15);
        return view('personnel',compact('administration','description','cles','carrousels'));

    }

    public function actualitecat($id)
    {   
        $mavar="";
        if($id=='T')
        {
            $mavar="Tracteur";
        }elseif($id=='M')
        {
            $mavar="Mécaniciens";
        }elseif($id=='A')
        {
            $mavar="Agriculture";
        }elseif($id=='I')
        {
            $mavar="Irrigation";
        }elseif($id=='O')
        {
            $mavar="Outils Attelé";
        }elseif($id=='P')
        {
            $mavar="Pièces détachées";
        }
         $carrousels = DB::table('carrousels')->whereIn('id', [4,5,6])->get();
         $description = DB::table('sitedatas')->first(); 
         $cles = DB::table('categorieactuelites')->get();
         $actualites = DB::table('actualites')->where('type', '=', $id)->orderBy('actualites.id','DESC')->paginate(6);
        /* $actualites = DB::table('actualites')
    		    ->join('categorieactuelites', 'actualites.type','=' , 'categorieactuelites.id')
                ->where('actualites.type', '=', $id)
                ->limit(2)
                ->paginate(2);*/
         return view('actualitecat',compact('mavar','actualites','description','cles','carrousels'));
    }

    public function documentation()
    {
         $carrousels = DB::table('carrousels')->whereIn('id', [4,5,6])->get();
         $description = DB::table('sitedatas')->first(); 
         $cles = DB::table('categorieactuelites')->get();
         $doc_pre = DB::table('services')
         ->join('documentations', 'services.id','=' , 'documentations.type')
         ->where('services.titre', '=', 'Pré-Récolte')
         ->limit(6)
         ->orderBy('documentations.id','DESC')
         ->get();
 

         $doc_post = DB::table('services')
            ->join('documentations', 'services.id','=' , 'documentations.type')
            ->where('services.titre', '=', 'Post-Récolte')
            ->limit(6)
            ->orderBy('documentations.id','DESC')
            ->get();


         $doc_num = DB::table('services')
         ->join('documentations', 'services.id','=' , 'documentations.type')
         ->where('services.titre', '=', 'Numérique')
         ->limit(6)
         ->orderBy('documentations.id','DESC')
         ->get();



       //  $documentations = DB::table('documentations')->orderBy('documentations.id','DESC')->paginate(2);
         return view('documentationcat',compact('doc_pre','doc_num','doc_post','description','cles','carrousels'));
    }

    public function newsletter()
    {
         $carrousels = DB::table('carrousels')->whereIn('id', [4,5,6])->get();
         $description = DB::table('sitedatas')->first(); 
         $cles = DB::table('categorieactuelites')->get();
         $documentations = DB::table('newsletters')->orderBy('newsletters.id','DESC')->paginate(6);
         return view('newsletter',compact('documentations','description','cles','carrousels'));
    }

    public function adminnewsletter()
    {
         return view('admin.postnewletter');
    }

    public function storenewsletter(Request $request)
    {
       /* $this->validate($request, [
            'email' => 'required|unique:subscribers,email',
        ]);*/
        $nemsletter = new Newsletter;
        $filesPath = [];
        $input = $request->all();
        $mytime = Carbon\Carbon::now()->format('FY');
        if ($request->hasFile('file') && $request->hasFile('image')) {
            $filen = $request->file('file')->getClientOriginalName();
            $files = time().'_'.$filen; 
            $images = $request->file('image')->getClientOriginalName();
            $images = time().'_'.$images; 
           $pathfile= $request->file('file')->storeAs('public/newletter/'.$mytime,$files);
           $pathimage= $request->file('image')->storeAs('public/newletter/'.$mytime,$images);
           
            array_push($filesPath, [
                'download_link' => 'newletter/'.$mytime.'/'.$files,
                'original_name' => $filen,
            ]);
     

        
 

          /* $filelink = array(
               'download_link' => 'newletter/'.$mytime.'/'.$files,
               "original_name"=> $filen
            );*/
            $nemsletter->image = 'newletter/'.$mytime.'/'.$images;
            //$nemsletter->file = json_encode($filelink , JSON_FORCE_OBJECT);
            $nemsletter->file = json_encode($filesPath);
            $nemsletter->titre = $request->titre;
            $nemsletter->parution = $request->parution;
            $nemsletter->courte_description = $request->courte_description;
        
            $nemsletter->save();
            Mail::to('developpeur@anama.bj')->bcc(Subscriber::pluck('email'))->send(new newsletterMail($nemsletter));
           // dd($nemsletter);
        }
        
      //  $data = array(
        //    'message' => "Vous avez souscrit à notre newsletter avec succès. Vous recevrez par mail nos prochaines parutions !"
        //);
       
     //   Mail::to($request->input('email'))->send(new sendingEmail($data));
     return redirect()->back()->with('message', 'Votre demande a été envoyée avec succès');
    }


}
