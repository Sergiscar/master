<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\sendingEmail;
use App\Mail\prestationEmail;
use App\Demandeprestation;

class ServiceController extends Controller
{
    public function index($id, $title)
    {
        $item = DB::table('actualites')->where('id', '=', $id)->first();
        $description = DB::table('sitedatas')->first(); 
        $cles = DB::table('categorieactuelites')->get();
        $carrousels = DB::table('carrousels')->whereIn('id', [4,5,6])->get();
        return view('actudetails',compact('item','description','cles','carrousels'));
    }
    public function service($id)
    {
        $item = DB::table('services')->where('id', '=', $id)->first();
       
        $description = DB::table('sitedatas')->first(); 
         $cles = DB::table('categorieactuelites')->get();
         $carrousels = DB::table('carrousels')->whereIn('id', [3])->get();
        return view('service',compact('item','description','cles','carrousels'));
    }

    public function servicepro()
    {    
        $cles = DB::table('categorieactuelites')->get();
         $item = DB::table('services')->where('proposition', '=', "SPEAMGCM")->first();
         $prestations = DB::table('services')
         ->join('prestations', 'services.id','=' , 'prestations.service')
         ->where('services.proposition', '=', "SPEAMGCM")
         ->orderBy('prestations.titre','ASC')
         ->get();
         $description = DB::table('sitedatas')->first();
         $carrousels = DB::table('carrousels')->whereIn('id', [4,5,6])->get();
        return view('production',compact('item','prestations','description','cles','carrousels'));
    }

    public function serviceing()
    {
        $cles = DB::table('categorieactuelites')->get();
         $item = DB::table('services')->where('proposition', '=', "SIRCAH")->first();
         $prestations = DB::table('services')
         ->join('prestations', 'services.id','=' , 'prestations.service')
         ->where('services.proposition', '=', "SIRCAH")
         ->orderBy('prestations.titre','ASC')
         ->get();
         $description = DB::table('sitedatas')->first();
         $carrousels = DB::table('carrousels')->whereIn('id', [4,5,6])->get();
        return view('ingenierie',compact('item','prestations','description','cles','carrousels'));
    }
    public function servicenum()
    {
        $cles = DB::table('categorieactuelites')->get();
         $item = DB::table('services')->where('proposition', '=', "SSI")->first();
         $prestations = DB::table('services')
         ->join('prestations', 'services.id','=' , 'prestations.service')
         ->where('services.proposition', '=', "SSI")
         ->orderBy('prestations.titre','ASC')
         ->get();
         $solutions = DB::table('solutions')->where('type_service', '=', $item->id)
         ->orderBy('id','ASC')
         ->get();
         $description = DB::table('sitedatas')->first();
         $carrousels = DB::table('carrousels')->whereIn('id', [4,5,6])->get();
        return view('numerique',compact('item','solutions','prestations','description','cles','carrousels'));
    }

    public function servicecom()
    {
        $cles = DB::table('categorieactuelites')->get();
         $item = DB::table('services')->where('proposition', '=', "SCM")->first();
         $prestations = DB::table('services')
         ->join('prestations', 'services.id','=' , 'prestations.service')
         ->where('services.proposition', '=', "SCM")
         ->orderBy('prestations.titre','ASC')
         ->get();
         $description = DB::table('sitedatas')->first();
         $carrousels = DB::table('carrousels')->whereIn('id', [4,5,6])->get();
        return view('commercial',compact('item','prestations','description','cles','carrousels'));
    }

    public function servicesav()
    {
        $cles = DB::table('categorieactuelites')->get();
         $item = DB::table('services')->where('proposition', '=', "SAV")->first();
         $description = DB::table('sitedatas')->first();
         $prestations = DB::table('services')
         ->join('prestations', 'services.id','=' , 'prestations.service')
         ->where('services.proposition', '=', "SAV")
         ->orderBy('prestations.titre','ASC')
         ->get();
         $carrousels = DB::table('carrousels')->whereIn('id', [4,5,6])->get();
        return view('sav',compact('item','prestations','description','cles','carrousels'));
    }
 

    public function ajaxGetPrestation(Request $request)
    {
        $prestations = DB::table('prestations')
        ->where('id', '=', $request->input('idprestation'))
        ->first();
        return json_encode($prestations);
    }

    public function saveDemandeIng(Request $request)
    {
        $input = $request->all();
        $prestation = DB::table('prestations')->where('id', '=', $request->input("prestation"))->first('titre');
        $data = array(
            'nom' => $request->input("nom"),
            'prenom' => $request->input("prenom"),
            'mail' => $request->input("mail"),
            'telephone' => $request->input("telephone"),
            'prestation' =>$prestation->titre,
            'montant' => $request->input("montant"),
            'quantite' => $request->input("quantite")
        );
        $program = Demandeprestation::create($input);
        Mail::to('franck.tchibozo@anama.bj')->cc('linh.nguyenquoc@anama.bj')->send(new prestationEmail($data));
        return redirect()->back()->with('message', 'Votre demande a été envoyée avec succès');
    }

    public function saveDemandesav(Request $request)
    {
        $input = $request->all();
        $prestation = DB::table('prestations')->where('id', '=', $request->input("prestation"))->first('titre');
        $data = array(
            'nom' => $request->input("nom"),
            'prenom' => $request->input("prenom"),
            'mail' => $request->input("mail"),
            'telephone' => $request->input("telephone"),
            'prestation' =>$prestation->titre,
            'montant' => $request->input("montant"),
            'quantite' => $request->input("quantite")
        );
        $program = Demandeprestation::create($input);
        Mail::to('beranger.assogba@anama.bj')->cc('rodrigue.adogony@anama.bj')->send(new prestationEmail($data));
        return redirect()->back()->with('message', 'Votre demande a été envoyée avec succès');
    }

    public function saveDemandessi(Request $request)
    {
        $input = $request->all();
        $prestation = DB::table('prestations')->where('id', '=', $request->input("prestation"))->first('titre');
        $data = array(
            'nom' => $request->input("nom"),
            'prenom' => $request->input("prenom"),
            'mail' => $request->input("mail"),
            'telephone' => $request->input("telephone"),
            'prestation' =>$prestation->titre,
            'montant' => $request->input("montant"),
            'quantite' => $request->input("quantite")
        );
        $program = Demandeprestation::create($input);
        Mail::to('vincent.zossou@anama.bj')->cc('linh.nguyenquoc@anama.bj')->send(new prestationEmail($data));
        return redirect()->back()->with('message', 'Votre demande a été envoyée avec succès');
    }
    public function saveDemandeScm(Request $request)
    {
        $input = $request->all();
        $prestation = DB::table('prestations')->where('id', '=', $request->input("prestation"))->first('titre');
        $data = array(
            'nom' => $request->input("nom"),
            'prenom' => $request->input("prenom"),
            'mail' => $request->input("mail"),
            'telephone' => $request->input("telephone"),
            'prestation' =>$prestation->titre,
            'montant' => $request->input("montant"),
            'quantite' => $request->input("quantite")
        );
        $program = Demandeprestation::create($input);
        Mail::to('rodrigue.adogony@anama.bj')->send(new prestationEmail($data));
        return redirect()->back()->with('message', 'Votre demande a été envoyée avec succès');
    }

    public function saveDemandePro(Request $request)
    {
        $input = $request->all();
        $prestation = DB::table('prestations')->where('id', '=', $request->input("prestation"))->first('titre');
        $data = array(
            'nom' => $request->input("nom"),
            'prenom' => $request->input("prenom"),
            'mail' => $request->input("mail"),
            'telephone' => $request->input("telephone"),
            'prestation' =>$prestation->titre,
            'montant' => $request->input("montant"),
            'quantite' => $request->input("quantite")
        );
         $program = Demandeprestation::create($input);
        Mail::to('frederic.balcaen@anama.bj')->cc('linh.nguyenquoc@anama.bj')->send(new prestationEmail($data));
        //Mail::to('serginodoleffo@gmail.com')->send(new prestationEmail($data));
        return redirect()->back()->with('message', 'Votre demande a été envoyée avec succès');
    }

    public function indexsolution($id){
        $cles = DB::table('categorieactuelites')->get();
        $solutions = DB::table('solutions')->where('id', '=', $id)->first();
        $description = DB::table('sitedatas')->first();
        $carrousels = DB::table('carrousels')->whereIn('id', [4,5,6])->get();
       return view('solutions',compact('solutions','description','cles','carrousels'));
    }




}
