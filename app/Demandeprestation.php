<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Demandeprestation extends Model
{
    use HasFactory;
    protected $fillable = ['nom','prenom','telephone','mail','quantite','prestation'];
}
