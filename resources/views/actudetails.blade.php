@extends('layouts.front')

@section('title')
    <title>SoNaMA - BENIN</title>
@endsection

@section('style')
 
@endsection


@section('content')
<section class="hero hero-home" >
            <div class="swiper-container hero-slider">
              <div class="swiper-wrapper">
                @foreach ($carrousels as $carousel)
                <div class="swiper-slide " style="height:500px">
                <div style="background: url('{{Voyager::image($carousel->image)}}');height:600px;  background-size: cover;" class="hero-content slide-content d-flex align-items-center">
                    <div class="container "  >
                      <div class="row text">
                          <h1 data-aos="fade-right" data-aos-delay="200">Nos Actualités
                                    </h1>
                      </div>
                     </div>
                  </div>
                </div>
                @endforeach
        
              </div>
            
            </div>
            <span class="arr-left"><i class="fa fa-angle-left"></i></span>
            <span class="arr-right"><i class="fa fa-angle-right"></i></span>
</section>
<div class="main-container container" >
  <div class="row mt-0" style="margin-left:1px; margin-right:1px;  background-color:#ffffff;">
  <section class="col-lg-8 col-md-8 col-sm-12 col-xs-12 " style="margin-bottom:5px;">
          
          <div class="container" style="margin-top:20px;" >
            <nav aria-label="breadcrumb" role="navigation">
              <ol class="breadcrumb">
                <li class="breadcrumb-item "><a style="color:#2e9141;" href="{{route('Accueil')}}">Accueil</a></li>
                <li aria-current="page" class="breadcrumb-item active">Informations</li>
                <li aria-current="page" class="breadcrumb-item active">Actualités</li>
                <li aria-current="page" class="breadcrumb-item active">Details</li>
              </ol>
            </nav>
           
            <h4 class="mb-5"> <?php echo "{$item->titre }" ?>  </h4>
            <div class="col-md-12"> <img src="{{Voyager::image($item->image)}}" alt="" class="img-fluid"></div>
            <div class="post-meta d-flex align-items-center flex-wrap mt-4" >   
              <div class="author d-flex align-items-center">
                <a href="#" class="name no-anchor-style"><i class="fa fa-user"></i>&nbsp;SoNaMA </a>
              </div>
              <div class="date ml-4"><i class="fa fa-calendar"></i> <time>{{Carbon\Carbon::parse($item->created_at)->format('d M Y')}}</time> </div>
              
            </div>
            <div class="text-content mt-3">
              <p class=""><?php echo "{$item->description }" ?> </p>
            </div>
            <div class="post-author mt-5">
              <div class="d-flex">
               <div class="info d-flex justify-content-between">
                  
                 Partager sur &nbsp;
                  <div class="right d-none d-sm-block">
                    <ul class="list-inline social">
                      <button class="sharer btn-primary" target="_blank" data-sharer="facebook" data-url="{{Request::url()}}"><i class="fa fa-facebook"></i></button>
                          <button class="sharer" data-sharer="twitter" style="background-color: #0081f5; color:white;" data-url="{{Request::url()}}">  <i class="fa fa-twitter"></i></button>
                        <button class="sharer btn-success" data-sharer="whatsapp "data-url="{{Request::url()}}">  <i class="fa fa-whatsapp"></i></button>
                      <button class="sharer btn-danger"data-sharer="email"  data-url="{{Request::url()}}"><i class="fa fa-google-plus"></i></button>
                     </ul>
                
                  </div>
                </div>
              </div>
             </div> 
        
      </section>

  <aside class="col-lg-4 col-md-4 col-sm-12 col-xs-12 fixed" style="margin-bottom:7px;">
      <section class="info-boxes pt-0 pb-0" style="margin-top:20px;">
<div class="card" style="background-color:#e9ecef;">
  <div class="card-header" style="background-color:color:#e9ecef;"><h5 style=" color:#777;">Catégories</h5></div>
  <div class="card-body">
    <div class="row">
      <div class="col-xs-12">
        <ul class="tags list-inline">
            @foreach ($cles as $cat)
            <li class="list-inline-item"><a href="{{route('Actualitecat',['id'=> $cat->id])}}" class="tag"><strong><?php echo "{$cat->nom } " ?></strong><strong></strong></a></li>
            @endforeach
        </ul>
      </div>
    </div>
  </div>
</div>
</section>

<br>
          <div class="card " style="background-color:#e9ecef;">
                    <div class="card-header text-center" style="background-color:#e9ecef;"><h5 style="color:#777;">Liens utiles</h5></div>
                    <div class="card-body" style="font-size: 13px;">
                      <div class="row">
                            <div class="px-3">
                              <p class="mt-1">
                                  <a target="_blank" href="#" class="text-black">
                                    Post-Récolte
                                  </a><br>
                              </p>
                              <hr>
                              <p class="mt-3">
                                  <a target="_blank" href="#" class="text-black">
                                    Pré-Récolte 
                                  </a><br>
                              </p> <hr>
                              <p class="mt-3">
                                  <a target="_blank" href="#" class="text-black">
                                    Prestations
                                  </a><br>
                              </p> <hr>
                              
                              <p class="mt-3">
                                  <a target="_blank" href="#" class="text-black">
                                    Miriaa
                                  </a><br>
                              </p> <hr>
                              <p class="mt-3">
                              <a target="_blank" href="#" class="text-black">
                                     Nous Contacter
                                  </a>
                              </p> <hr>
                            </div>
                      
                    </div>
          </div>
          
      </aside>
  </div>      
    </div>
    @endsection