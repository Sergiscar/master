<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
        
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="icon" type="image/png" href="assets/images/benin.png"/>
    <!-- Document title -->
    @yield('title')
    <!-- Stylesheets & Fonts -->
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,700,700i%7CRajdhani:400,600,700"
        rel="stylesheet">
    <!-- Plugins Stylesheets -->
    
    <link rel="stylesheet" href="{{asset('assets/css/loader/loaders.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/font-awesome/font-awesome.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/aos/aos.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/swiper/swiper.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/lightgallery.min.css')}}">
    <!-- Template Stylesheet -->
    <link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
    <!-- Responsive Stylesheet -->
    <link rel="stylesheet" href="{{asset('assets/css/responsive.css')}}">

        @yield('style')

    </head>
<body>

<!-- Loader Start -->
<!--div class="css-loader">
        <div class="loader-inner line-scale d-flex align-items-center justify-content-center"></div>
    </div-->
    @if(session()->has('message'))
    <div class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif

<div class="app-container">
    <div class="fadetoblack visible-xs"></div>
    <div class="row content-container">
    <nav class="navbar navbar-default navbar-fixed-top navbar-top">
    <div class="container-fluid">
        <div class="navbar-header">
       
            <ol class="breadcrumb hidden-xs">
                
                    <li class="active">
                        <a href="{{ route('voyager.dashboard')}}"><i class="voyager-boat"></i> {{ __('voyager::generic.dashboard') }}</a>
                    </li>
              
            </ol>
     
        </div>
           </div>
</nav>
    
        <!-- Begin page -->
      
            <!-- Left Sidebar End -->

            <!-- Start right Content here -->

            @yield('content')

            <!-- End Right content here -->

        <!-- END wrapper -->
      <!-- jQuery  -->
    <script src="{{asset('assets/js/jquery-3.3.1.js')}}"></script>
    <!--Plugins-->
    <script src="{{asset('assets/js/bootstrap.bundle.js')}}"></script>
    <script src="{{asset('assets/js/loaders.css.js')}}"></script>
    <script src="{{asset('assets/js/aos.js')}}"></script>
    <script src="{{asset('assets/js/swiper.min.js')}}"></script>
    <script src="{{asset('assets/js/lightgallery-all.min.js')}}"></script>
    <!--Template Script-->
    <script src="{{asset('assets/js/main.js')}}"></script>
    <script src="{{ asset('assets/sharer/sharer.min.js') }}"></script>
    <script>



    /*$("#toTop").click(function() {
    $("html, body").animate({scrollTop: 0}, 1000);
    });*/
  </script>


    @yield('script')
   
</body>
</html>
