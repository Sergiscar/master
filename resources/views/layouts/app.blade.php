<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
        
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="icon" type="image/png" href="assets/images/benin.png"/>
    <!-- Document title -->
    @yield('title')
    <!-- Stylesheets & Fonts -->
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,700,700i%7CRajdhani:400,600,700"
        rel="stylesheet">
    <!-- Plugins Stylesheets -->
    
    <link rel="stylesheet" href="{{asset('assets/css/loader/loaders.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/font-awesome/font-awesome.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/aos/aos.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/swiper/swiper.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/lightgallery.min.css')}}">
    <!-- Template Stylesheet -->
    <link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
    <!-- Responsive Stylesheet -->
    <link rel="stylesheet" href="{{asset('assets/css/responsive.css')}}">
    <style>
        .space-bottom-s {
    padding-bottom: 1.25rem;
}
 
.my-banner.sticky .containere {
    
    position: fixed;
    bottom: 0;
    left: 0;
    right: 0;
    background:#ffffff solid;
    z-index: 99;
    border-top: 5px solid #b21d0a;
    transition: transform .1s ease-in-out;

}
.my-banner .containere {
    padding: 0.625rem 2.1875rem;
    background: #fff;
    border-top: 1px solid #d8d8d8;
}
.border-bottom {
    border-bottom: 1px solid #d8d8d8;
}
.my-banner.sticky .btn-close {
    display: block;
}
.my-banner .btn-close {
    display: none;
    position: absolute;
    top: 0.625rem;
    right: 0.625rem;
    padding: 0.625rem 0.625rem 1.25rem 1.25rem;
}
    .my-banner .containere--thicker {
    max-width: 100%;
    padding: 0;
}
.banner-contact-w {
    border: none;
    padding-top: 0.3125rem;
    padding-bottom: 0.3125rem;
}
.bannerContact-desc {
    font-size: .875rem;
    line-height: 1.125rem;
}
.banner-contact-w header p {
    margin-bottom: 0.875rem;
    font-size: .6875rem;
    line-height: 1.2;
}

.form .field {
    position: relative;
    width: 100%;
}

.field.nl input:invalid, .field.nl input:required:invalid {
    outline: none;
    border: none;
    box-shadow: none;
}

.form.rounded input {
    padding-left: 3.75rem
;
}
.form.rounded>div, .form.rounded input {
    border-radius: 20em
;
}
.field.nl input {
    padding-left: 3.75rem;
    background: none;
}
.form input {
    background-color: transparent;
    border: none;
    color: #000;
    font-size: .9375rem;
    padding: 1.25rem 0.5625rem 0.9375rem 2.0625rem;
    width: 100%;
    min-height: 3.75rem;
}
button, input {
    overflow: visible;
}
button, input, optgroup, select, textarea {
    font-family: inherit;
    font-size: 100%;
    line-height: 1.15;
    margin: 0;
}
.bannerContact-formcontainere .form label {
    font-size: .8125rem;
}
.form label {
    font-size: .9375rem;
    position: absolute;
    top: 1.25rem;
    left: 2rem;
    transition: all .1s ease-in-out;
}


.banner-contact-block header p a {
    text-decoration: underline;
}
.form button {
    padding: 0 1.6875rem;
    background: url(/assets/images/arrow-right-black.5b753183.svg) no-repeat 50%;
    border: 3px solid transparent;
    border-top-color: transparent;
    border-top-style: solid;
    border-top-width: 3px;
    border-right-color: transparent;
    border-right-style: solid;
    border-right-width: 3px;
    border-bottom-color: transparent;
    border-bottom-style: solid;
    border-bottom-width: 3px;
    border-left-color: transparent;
    border-left-style: solid;
    border-left-width: 3px;
    border-image-source: initial;
    border-image-slice: initial;
    border-image-width: initial;
    border-image-outset: initial;
    border-image-repeat: initial;
    margin: -1px;
}

.banner-contact {
    display: flex;
    justify-content: space-between;
    border-top: 1px solid #d8d8d8;
    border-bottom: 1px solid #d8d8d8;
}
banner-contact-block header {
    padding-right: 0;
    text-align: left;
}
.banner-contact header {
    flex-basis: 50%;
    padding-right: 10%;
}
.t-align-c {
    text-align: center;
}
.banner-contact-block header h2 {
    line-height: 1.875rem;
}
.banner-contact-w header h2 {
    font-family: Aiglon Bold,Arial,sans-serif;
    font-size: 1.375rem;
    line-height: 1.875rem;
    padding-right: 1.25rem;
}
 @media (min-width: 64rem){
 .my-banner .banner-contact__elmts {
    padding: 0 1.25
rem
;
}
}
@media (min-width: 60.5rem){
.my-banner .banner-contact__elmts {
    flex-basis: 40%;
    margin-left: 0;
}
}
@media  (min-width: 75.0625rem){
.bannerContact-formcontainere {
    padding: 0;
}
}
.bannerContact-formcontainere {
    padding: 0 1.25rem;
}

@media(min-width: 1024px){
.banner-contact-block__elmts {
    margin-left: 3.125rem;
    justify-content: space-between;
  }
}
.banner-contact__elmts {
    display: flex;
    flex-basis: 50%;
    justify-content: flex-end;
}
.banner-contact form {
  position: relative;
  align-self: center;
  flex-grow: 1;
  margin-right: 1.25rem;
}
.form.rounded>div, .form.rounded input {
  border-radius: 20em;
}
.form>div {
  border: 1px
solid #d8d8d8;
}
.d-flex {
  display: flex!important;
}

   
</style>
        @yield('style')

    </head>
<body>
@if(session()->has('message'))
    <div class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif
<!-- Loader Start -->
<!--div class="css-loader">
        <div class="loader-inner line-scale d-flex align-items-center justify-content-center"></div>
    </div-->
    
    @include('partials.header',['description' => $description, 'cles' => $cles])
    
    <div class="my-banner sticky show" id="myscroll">
                        <div class="containere border-bottom space-bottom-s ">
                            <button type="button"  class="close" onclick="retrive();">
                            <span class="close">&times;</span>
                            </button>
                             <div class="containere--thicker banner-contact banner-contact-w banner-contact-block">
                                <header class="t-align-c">
                                  <h2>Inscrivez-vous à notre lettre d'information</h2>
                                      <p class="bannerContact-desc">
                                           Recevez chaque mois du contenu exclusif.
                                          <a href="{{route('Newsletter')}}">
                                          Nos anciennes parutions.                   
                                          </a>
                                       </p>
                                </header>
                               <div class="banner-contact__elmts banner-contact-block__elmts bannerContact-formcontainere">
                                <form id="myform" class="rounded form validate" role="form" action="/addmail" method="post"   >
                                {{csrf_field()}}
                                   <div class="d-flex">
                                       <div class="field nl">
                                           <input class="floating-label" type="email" autocomplete="email" placeholder="Votre adresse e-mail : prenom.nom@domaine.fr" required="" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" id="email" name="email"  data-errmsg=" : format incorrect">
                        
                                       </div>
                                      <button type="submit" title="Confirm registration"></button>
                                    </div>
                                </form>
                               </div>
                             </div>
                          </div>
                </div>
            
  
        <!-- Begin page -->
      
            <!-- Left Sidebar End -->

            <!-- Start right Content here -->

            @yield('content')

           @include('partials.footer',['description' => $description])
            <!-- End Right content here -->

        <!-- END wrapper -->
      <!-- jQuery  -->
    <script src="{{asset('assets/js/jquery-3.3.1.js')}}"></script>
    <!--Plugins-->
    <script src="{{asset('assets/js/bootstrap.bundle.js')}}"></script>
    <script src="{{asset('assets/js/loaders.css.js')}}"></script>
    <script src="{{asset('assets/js/aos.js')}}"></script>
    <script src="{{asset('assets/js/swiper.min.js')}}"></script>
    <script src="{{asset('assets/js/lightgallery-all.min.js')}}"></script>
    <!--Template Script-->
    <script src="{{asset('assets/js/main.js')}}"></script>
    <script src="{{ asset('assets/sharer/sharer.min.js') }}"></script>
    <script>
  mybutton = document.getElementById("myscroll");

// When the user scrolls down 20px from the top of the document, show the button
window.onscroll = function() {scrollFunction()};

function scrollFunction() {
  if (document.body.scrollTop > 100 || document.documentElement.scrollTop > 100) {
    mybutton.style.display = "block";
  } else {
    mybutton.style.display = "none";
  }
}


// When the user clicks on the button, scroll to the top of the document
function topFunction() {
  document.body.scrollTop = 0; // For Safari
  document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
}

function retrive() {
    mybutton.parentNode.removeChild(mybutton);
    return false;
}

</script>
<div style="margin-bottom:80px">
</div>
    @yield('script')
   
</body>
</html>
