@extends('layouts.front')

@section('title')
    <title>SoNaMA - BENIN</title>
@endsection

@section('style')
 
@endsection


@section('content')
<section class="hero hero-home" >
            <div class="swiper-container hero-slider">
              <div class="swiper-wrapper">
                @foreach ($carrousels as $carousel)
                <div class="swiper-slide " style="height:500px">
                <div style="background: url('{{Voyager::image($carousel->image)}}');height:600px;  background-size: cover;" class="hero-content slide-content d-flex align-items-center">
                  
                  </div>
                </div>
                @endforeach
        
              </div>
            
            </div>
            <span class="arr-left"><i class="fa fa-angle-left"></i></span>
            <span class="arr-right"><i class="fa fa-angle-right"></i></span>
</section>

<div class="main-container container" >
  <div class="row mt-0" style="margin-left:1px; margin-right:1px;  background-color:#ffffff;">
  <section class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="margin-bottom:5px;">
  <div class="container" style="margin-top:20px;" >
            <nav aria-label="breadcrumb" role="navigation">
              <ol class="breadcrumb">
                <li class="breadcrumb-item "><a style="color:#2e9141;" href="{{route('Accueil')}}">Accueil</a></li>
                <li aria-current="page" class="breadcrumb-item active">Société</li>
                <li aria-current="page" class="breadcrumb-item active">Personnel</li>
              </ol>
            </nav>
           
         
            </div>
      </div>      
    </div>
        
            <div class="container">
            <h3 class="text-center">PERSONNEL</h3><hr> <br>
                    <div class="row"  style="text-align:center">
                      @foreach($administration as $post)    
                     
                      <div class="col-lg-4 col-md-4 col-xl-4 d-flex justify-content-center" style="margin-bottom:30px;">
                        <div class="staff-member">
                        @if(empty($post->photo) && (empty($post->genre)||$post->genre=="M")  )
                          <img src="{{asset('assets/images/hommedeux.png')}}"  class="img-fluid"  alt="{{$post->poste}}">
                          <div class="info">
                             <span style="color:black">M. <?php echo "{$post->nom }"?></span>
                             <h6 style="color:rgba(46, 145, 65, 1)"><?php echo "{$post->mail }"?></h6>
                             <h5 style="color:black; text-size:20px; padding: 0px 30px 0px 30px;"><?php echo "{$post->poste }"?></h5>
                          </div>
                          @elseif(empty($post->photo)&& $post->genre=="F")
                          <img src="{{asset('assets/images/femmedeux.png')}}"  class="img-fluid"  alt="{{$post->poste}}">
                          <div class="info">
                             <span style="color:black">Mme <?php  echo "{$post->nom }"?></span>
                             <h6 style="color:rgba(46, 145, 65, 1)"><?php echo "{$post->mail }"?></h6>
                             <h5 style="color:black; text-size:20px; padding: 0px 30px 0px 30px;" class="h5 teacher mb-0"><?php echo "{$post->poste }"?></h5>
                          </div>
                          @elseif(!empty($post->photo))
                          <img src="{{asset('assets/images/hommedeux.png')}}"  class="img-fluid" height="150px" width="200px"  alt="{{$post->poste}}">
                          <div class="info">
                             <span style="color:black">M. <?php echo "{$post->nom }"?></span>
                             <h6 style="color:rgba(46, 145, 65, 1)"><?php echo "{$post->mail }"?></h6>
                             <h5 style="color:black; text-size:20px; padding: 0px 30px 0px 30px;"><?php echo "{$post->poste }"?></h5>
                          </div>
                          @endif
                       
                         
                         
                      </div>
                      </div>
                      @endforeach
                   
                    </div>
                   <div class="overlay d-flex align-items-center justify-content-center">
                          {{$administration->links('pagination::bootstrap-4')}}
                   </div>

                  </div>
        
@endsection