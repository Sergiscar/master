@extends('layouts.app')

@section('title')
    <title>SoNaMA - BENIN</title>
@endsection

@section('style')
 
@endsection


@section('content')
    <!-- Hero Start -->
    @if ($message = Session::get('success'))
                            <div class="alert alert-success">
                                <p>{{ $message }}</p>
                            </div>
  @endif
          <!-- Hero Section-->
          <section class="hero hero-home" >
            <div class="swiper-container hero-slider">
              <div class="swiper-wrapper">
                @foreach ($carrousels as $carousel)
                <div class="swiper-slide " style="height:600px;">
                <div style="background: url('{{Voyager::image($carousel->image)}}');height:600px;  background-size: cover;" class="hero-content slide-content d-flex align-items-center">
                    <div class="container "  >
                      <div class="row text">
                          <h1 data-aos="fade-right" data-aos-delay="200">Reveler <br>le potentiel de la terre
                                    </h1>
                      </div>
                     </div>
                  </div>
                </div>
                @endforeach
              </div>
            </div>
            <span class="arr-left"><i class="fa fa-angle-left"></i></span>
            <span class="arr-right"><i class="fa fa-angle-right"></i></span>
          </section>
    <section class="cta-propos" >
        <div class="container">
        <div class="title text-center">
                <h5 class="title-blue">À propos</h5>
            </div>
            
            <div >
                <div class="content text-center" >
                    <p class="text-center">  <?php  echo "{$post->debut_description  }"?>  </p>
                    <a class="lire" href="{{route('Societe')}}">Lire plus...</a>
                </div>
               
            </div>
            
        </div>
    </section>
  
    <section class="services">
        <div class="container">
            <div class="title text-center">
                <h1 class="title-blue">Nos services</h1>
            </div>
            <div class="container">
                <div class="row">
                
                    <div class="col-sm-6 col-lg-4">
                        <div class="media" data-aos="fade-up" data-aos-delay="200" data-aos-duration="400">
                        <a class="post-btn" href="{{route('Ingenierie')}}">         <img class="mr-4" src="{{Voyager::image($ingerierie->image)}}" alt="Web Development"> </a>
                            <div class="media-body">
                            <a class="post-btn" href="{{route('Ingenierie')}}">  <h6> {!! $ingerierie->titre !!}</h6></a>
                                {!! $ingerierie->courte_description !!}
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6 col-lg-4">
                        <div class="media" data-aos="fade-up" data-aos-delay="200" data-aos-duration="400">
                        <a class="post-btn" href="{{route('Production')}}">         <img class="mr-4" src="{{Voyager::image($production->image)}}" alt="Web Development"> </a>
                            <div class="media-body">
                            <a class="post-btn" href="{{route('Production')}}">     <h6> {!! $production->titre !!}</h6> </a>
                                {!! $production->courte_description !!}
                            </div>
                        </div>
                    </div>
               
                    <div class="col-sm-6 col-lg-4">
                        <div class="media" data-aos="fade-up" data-aos-delay="200" data-aos-duration="400">
                        <a class="post-btn" href="{{route('Numerique')}}">         <img class="mr-4" src="{{Voyager::image($numerique->image)}}" alt="Web Development"> </a>
                            <div class="media-body" >
                            <a class="post-btn" href="{{route('Numerique')}}"> <h6> {!! $numerique->titre !!}</h6> </a>
                                {!! $numerique->courte_description !!}
                            </div>
                        </div>
                    </div>
               
                    <div class="col-sm-6 col-lg-4">
                        <div class="media" data-aos="fade-up" data-aos-delay="200" data-aos-duration="400">
                        <a class="post-btn" href="{{route('Commercial')}}">         <img class="mr-4" src="{{Voyager::image($commercial->image)}}" alt="Web Development"> </a>
                            <div class="media-body">
                            <a class="post-btn" href="{{route('Commercial')}}"> <h6> {!! $commercial->titre !!}</h6> </a>
                                {!! $commercial->courte_description !!}
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6 col-lg-4">
                        <div class="media" data-aos="fade-up" data-aos-delay="200" data-aos-duration="400">
                        <a class="post-btn" href="{{route('Sav')}}">         <img class="mr-4" src="{{Voyager::image($sav->image)}}" alt="Web Development"> </a>
                            <div class="media-body">
                            <a class="post-btn" href="{{route('Sav')}}"> <h6> {!! $sav->titre !!}</h6> </a>
                                {!! $sav->courte_description !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
 
    <section class="recent-posts">
        <div class="container">
        <div class="title text-center">
                <h1 class="title-blue">Actualités</h1>
            </div>
            <div class="row">
            <?php $test=0; ?>
            @foreach ($actualites as $actualite)
            <?php $test++; ?>
            @if($test&1)
            <div class="col-lg-6">
                    <div class="single-rpost d-sm-flex align-items-center" data-aos="fade-right"
                        data-aos-duration="800">
                        <div class="post-content text-sm-right">
                           <time>{{Carbon\Carbon::parse($actualite->created_at)->format('d M Y')}}</time>
                           <?php $titre2 = str_replace(" ", "-", $actualite->titre); ?>
                            <h5><a href="{{route('Actualitedetail',['id'=> $actualite->id,'title'=>$titre2])}}">{!! $actualite->titre !!}</a></h5>
                            <p>{!! $actualite->courte_description !!}</p>
                           
                            <a class="post-btn" href="{{route('Actualitedetail',['id'=> $actualite->id,'title'=>$titre2])}}"><i class="fa fa-arrow-right"></i></a>
                        </div>
                        <div class="post-thumb">
                            <img class="img-fluid" src="{{Voyager::image($actualite->image)}}" alt="Post 1">
                        </div>
                    </div>
                </div>
              
            @else
            <div class="col-lg-6">
                    <div class="single-rpost d-sm-flex align-items-center" data-aos="fade-left" data-aos-duration="800">
                        <div class="post-thumb">
                            <img class="img-fluid" src="{{Voyager::image($actualite->image)}}" alt="Post 1">
                        </div>
                        <div class="post-content">
                        <?php $titre2 = str_replace(" ", "-", $actualite->titre); ?>
                            <time datetime="2019-04-06T13:53">{{Carbon\Carbon::parse($actualite->created_at)->format('d M Y')}}</time>
                            <h5><a href="{{route('Actualitedetail',['id'=> $actualite->id,'title'=>$titre2])}}">{!! $actualite->titre !!}</a></h5>
                            <p>{!! $actualite->courte_description !!}</p>
                          
                            <a class="post-btn" href="{{route('Actualitedetail',['id'=> $actualite->id,'title'=>$titre2])}}"><i class="fa fa-arrow-right"></i></a>
                        </div>
                    </div>
                </div>
            @endif  
            @endforeach
            </div>
            <div class="text-center">
                <a href="{{route('Actualite')}}" class=" btn btna  btn-primary">Voir Plus</a>
            </div>
        </div>
    </section>
   
    <section class="testimonial-and-clients">
      <div class="container">
        <div class="title text-center">
                <h1 class="title-white">Nos partenaires</h1>
            </div>
            <div class="clients" data-aos="fade-up" data-aos-delay="200" data-aos-duration="600">
                <div class="swiper-container clients-slider">
                    <div class="swiper-wrapper">
                    @foreach ($partenaires as $partenaire)
                        <div class="swiper-slide">
                            <a   href="{{$partenaire->site_web}}" target="_blank" >
                            <img src="{{Voyager::image($partenaire->logo)}}" alt="Client 1">
                            </a>
                        </div>
                        @endforeach
                        <div class="swiper-slide">
                        </div>
                        <div class="swiper-slide">
                        </div>
                        <div class="swiper-slide">
                        </div>
                        <div class="swiper-slide">
                        </div>


                      
                    </div>
                
                        <!--div class="swiper-slide">
                        </div-->
                </div>
        </div>
      </div>
    </section>
    
@endsection