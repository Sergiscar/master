@extends('layouts.front')

@section('title')
    <title>SoNaMA - BENIN</title>
@endsection

@section('style')
<style>
body {
    color: #000;
    overflow-x: hidden;
    height: 100%;
   
    background-repeat: no-repeat;
    background-size: 100% 100%
}

.card {
    padding: 30px 40px;
    margin-top: 60px;
    margin-bottom: 60px;
    border: none !important;
    box-shadow: 0 6px 12px 0 rgba(0, 0, 0, 0.2)
}

.blue-text {
    color: #00BCD4
}

.form-control-label {
    margin-bottom: 0
}

input,
textarea,
button {
    padding: 8px 15px;
    border-radius: 5px !important;
    margin: 5px 0px;
    box-sizing: border-box;
    border: 1px solid #ccc;
    font-size: 18px !important;
    font-weight: 300
}

input:focus,
textarea:focus {
    -moz-box-shadow: none !important;
    -webkit-box-shadow: none !important;
    box-shadow: none !important;
    border: 1px solid #00BCD4;
    outline-width: 0;
    font-weight: 400
}

.btn-block {
    text-trquantiteform: uppercase;
    font-size: 15px !important;
    font-weight: 400;
    height: 43px;
    cursor: pointer
}

.btn-block:hover {
    color: #fff !important
}

button:focus {
    -moz-box-shadow: none !important;
    -webkit-box-shadow: none !important;
    box-shadow: none !important;
    outline-width: 0
}
</style>
@endsection


@section('content')
<section class="hero hero-home" >
            <div class="swiper-container hero-slider">
              <div class="swiper-wrapper">
                @foreach ($carrousels as $carousel)
                <div class="swiper-slide " style="height:500px">
                <div style="background: url('{{Voyager::image($carousel->image)}}');height:600px;  background-size: cover;" class="hero-content slide-content d-flex align-items-center">
                    </div>
                </div>
                @endforeach
              </div>
            
            </div>
            <span class="arr-left"><i class="fa fa-angle-left"></i></span>
            <span class="arr-right"><i class="fa fa-angle-right"></i></span>
</section>

<div class="main-container container" >
  <div class="row mt-0" style="margin-left:1px; margin-right:1px;  background-color:#ffffff;">
  <section class="col-lg-8 col-md-8 col-sm-12 col-xs-12 " style="margin-bottom:5px;">
  <div class="container" style="margin-top:20px;" >
            <nav aria-label="breadcrumb" role="navigation">
              <ol class="breadcrumb">
                <li class="breadcrumb-item "><a style="color:#2e9141;" href="{{route('Accueil')}}">Accueil</a></li>
                <li aria-current="page" class="breadcrumb-item active">Services</li>
                <li aria-current="page" class="breadcrumb-item active">Numérique</li>
              </ol>
            </nav>
        
            <div class="row"> <div class="col-md-1"><img src="{{Voyager::image($item->image)}}" alt="" class="img-fluid"> </div>   <div class="col-md-6">   <h4 class="mb-5">  <?php echo "{$item->titre }" ?> </div> </h4></div>
            <div class="text-content mt-2">
              <?php echo "{$item->bref }" ?> 
              <ul> 
              @foreach ($solutions as $solution)
              <li> 
              <?php $titre2 = str_replace(" ", "-", $solution->nom); ?>
              <a href="{{route('Solution',['id'=> $solution->id,'titre'=> $titre2 ])}}">{!! $solution->nom !!}</a>
              </li>
              @endforeach
              </ul> 
            </div>
            <h5 class="text-center mb-4">Demander une prestation</h5>
            <form class="form-card" action="{{route('Demandeprestationssi')}}" method="POST" >
                    @csrf
                    <div class="row justify-content-between text-left">
                        <div class="form-group col-sm-6 flex-column d-flex"> <label class="form-control-label px-3">Nom<span class="text-danger"> *</span></label> <input type="text" id="nom" name="nom" placeholder="" onblur="validate(1)"> </div>
                        <div class="form-group col-sm-6 flex-column d-flex"> <label class="form-control-label px-3">Prénoms<span class="text-danger"> *</span></label> <input type="text" id="prenom" name="prenom" placeholder="" onblur="validate(2)"> </div>
                    </div>
                    <div class="row justify-content-between text-left">
                        <div class="form-group col-sm-6 flex-column d-flex"> <label class="form-control-label px-3">Télephone<span class="text-danger"> *</span></label> <input type="text" id="telephone" name="telephone" id="mail" name="mail" placeholder="De préference whatsapp" onblur="validate(3)"> </div>
                        <div class="form-group col-sm-6 flex-column d-flex"> <label class="form-control-label px-3">mail<span class="text-danger"> </span></label> <input type="mail" id="mail" name="mail" placeholder="" onblur="validate(4)"> </div>
                    </div>
                    <div class="row justify-content-between text-left">
                        <div class="form-group col-sm-12 flex-column d-flex">
                        <label class="form-control-label px-3">Prestations<span class="text-danger"> *</span></label> 
                           <select name="prestation" id="idprestation">
                              <option>Sélectionnez</option>
                                   @foreach( $prestations as $prestation)
                                   <option value='{{ $prestation->id }}' >{{ $prestation->titre }}</option>
                                    @endforeach
                           </select>
                       </div>
                    </div>

                    <div class="row justify-content-between text-left">
                        <div class="form-group col-6 flex-column d-flex"> <label class="form-control-label px-3">Montant<span class="text-danger"> </span></label> <input type="text" id="idmontant" name="montant" readonly placeholder="" > </div>
                        <div class="form-group col-6 flex-column d-flex"> <label class="form-control-label px-3">Quantité<span class="text-danger"> *</span></label> <input type="number" id="quantite" name="quantite" placeholder="" onblur="validate(6)"> </div>
                    </div>
                    <div class="row justify-content-end">
                        <div class="form-group col-sm-6"> <button type="submit" class="btn-block btn-success">Envoyer</button> </div>
                    </div>
            </form>

            <div class="post-author mt-5">
              <div class="d-flex">
               <div class="info d-flex justify-content-between">
                  
               <h5 class="mb-2">Partager sur &nbsp;</h5>
                  <div class="right d-none d-sm-block">
                    <ul class="list-inline social">
                      <button class="sharer btn-primary" target="_blank" data-sharer="facebook" data-url="{{Request::url()}}"><i class="fa fa-facebook"></i></button>
                          <button class="sharer" data-sharer="twitter" style="background-color: #0081f5; color:white;" data-url="{{Request::url()}}">  <i class="fa fa-twitter"></i></button>
                        <button class="sharer btn-success" data-sharer="whatsapp "data-url="{{Request::url()}}">  <i class="fa fa-whatsapp"></i></button>
                      <button class="sharer btn-danger"data-sharer="email"  data-url="{{Request::url()}}"><i class="fa fa-google-plus"></i></button>
                     </ul>
                
                  </div>
                </div>
              </div>
             </div> 
        
      </section>

      <aside class="col-lg-4 col-md-4 col-sm-12 col-xs-12 fixed" style="margin-bottom:7px; margin-top:20px;">
      <div class="card" style="background-color:#e9ecef;">
                    <div class="card-header text-center" style="background-color:#e9ecef;"><h5 style="color:#777;">Liens utiles</h5></div>
                    <div class="card-body" style="font-size: 13px;">
                      <div class="row">
                            <div class="px-3">
                              <p class="mt-1">
                                  <a target="_blank" href="#" class="text-black">
                                    Post-Récolte
                                  </a><br>
                              </p>
                              <hr>
                              <p class="mt-3">
                                  <a target="_blank" href="#" class="text-black">
                                    Pré-Récolte 
                                  </a><br>
                              </p> <hr>
                              <p class="mt-3">
                                  <a target="_blank" href="#" class="text-black">
                                    Prestations
                                  </a><br>
                              </p> <hr>
                              
                              <p class="mt-3">
                                  <a target="_blank" href="#" class="text-black">
                                    Miriaa
                                  </a><br>
                              </p> <hr>
                              <p class="mt-3">
                              <a target="_blank" href="#" class="text-black">
                                     Nous Contacter
                                  </a>
                              </p> <hr>
                            </div>
                      
                    </div>
          </div>
          
      </aside>
      </div>      
    </div>
    @section('script')
<script> 
var $idprestation = $('#idprestation');
var $idmontant = $('#idmontant');
var $idquantite = $('#quantite');
var $idprestation = $('#idprestation');
var $idmontant = $('#idmontant');
var $idquantite = $('#quantite');
var $montantinit =0;
$idprestation.on('change', function() {
  document.getElementById("idmontant").value=""; 
  document.getElementById("quantite").value=0; 
   $idquantite.empty(); 

   var $prestation = $idprestation.val();
    $.ajax({
        url: '{{route('ajaxGetPrestation')}}',
        data: 'idprestation='+$(this).val(), // on envoie $_GET['go']
        dataType: 'json', // on veut un retour JSON
        success: function(valuee) {
          if (valuee === null)
          {
            document.getElementById("idmontant").value=""
          }
          else{
            document.getElementById("idmontant").value = valuee.montant;
            if (valuee.montant=="Néant")
            {

            } else{
            $montantinit=valuee.montant;
            }
          }
                
        },
       error: function(json)
                {
                    document.getElementById("idmontant").value=""
                }
    });
});
$idquantite.on('change', function() { 
   var $quantite = $idquantite.val();
   var $montant = $idmontant.val();
   if ($montant=="Néant")
   {

   } else{
     console.log($montant);
     console.log($quantite);
    document.getElementById("idmontant").value = $quantite * $montantinit ;
   }

});

function validate(val) {
v1 = document.getElementById("nom");
v2 = document.getElementById("prenom");
v3 = document.getElementById("mail");
v4 = document.getElementById("telephone");
v5 = document.getElementById("job");
v6 = document.getElementById("quantite");

flag1 = true;
flag2 = true;
flag3 = true;
flag4 = true;
flag5 = true;
flag6 = true;

if(val>=1 || val==0) {
if(v1.value == "") {
v1.style.borderColor = "red";
flag1 = false;
}
else {
v1.style.borderColor = "green";
flag1 = true;
}
}

if(val>=2 || val==0) {
if(v2.value == "") {
v2.style.borderColor = "red";
flag2 = false;
}
else {
v2.style.borderColor = "green";
flag2 = true;
}
}
if(val>=3 || val==0) {
if(v3.value == "") {
v3.style.borderColor = "red";
flag3 = false;
}
else {
v3.style.borderColor = "green";
flag3 = true;
}
}
if(val>=4 || val==0) {
if(v4.value == "") {
v4.style.borderColor = "red";
flag4 = false;
}
else {
v4.style.borderColor = "green";
flag4 = true;
}
}
if(val>=6 || val==0) {
if(v6.value == "") {
v6.style.borderColor = "red";
flag6 = false;
}
else {
v6.style.borderColor = "green";
flag6 = true;
}
}

flag = flag1 && flag2 && flag3 && flag4 && flag5 && flag6;

return flag;
}
</script>
@endsection

@endsection