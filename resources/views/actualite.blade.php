@extends('layouts.app')

@section('title')
    <title>SoNaMA - BENIN</title>
@endsection

@section('style')
<style>
.latest-news .news-block .news-block-inner{padding:30px;margin-top:30px;position:relative}
.latest-news .news-block .bg-image{background-size:cover !important;color:#fff;z-index:1}
.latest-news .news-block .bg-image::before{content:'';width:100%;height:100%;background:rgba(34,34,34,0.59);position:absolute;top:0;left:0;z-index:-1}
.latest-news .news-block .bg-image p{color:#fff}
.latest-news .news-block p{margin:20px 0;color:#555}
</style>
@endsection


@section('content')
<section class="hero hero-home" >
            <div class="swiper-container hero-slider">
              <div class="swiper-wrapper">
                @foreach ($carrousels as $carousel)
                <div class="swiper-slide " style="height:500px">
                <div style="background: url('{{Voyager::image($carousel->image)}}');height:600px;  background-size: cover;" class="hero-content slide-content d-flex align-items-center">
                    <div class="container "  >
                      <div class="row text">
                          <h1 data-aos="fade-right" data-aos-delay="200">Nos Actualités
                                    </h1>
                      </div>
                     </div>
                  </div>
                </div>
                @endforeach
        
              </div>
            
            </div>
            <span class="arr-left"><i class="fa fa-angle-left"></i></span>
            <span class="arr-right"><i class="fa fa-angle-right"></i></span>
</section>
<div class="main-container container" >
  <div class="row mt-0" style="margin-left:1px; margin-right:1px;  background-color:#ffffff;">
  <section class="col-lg-8 col-md-8 col-sm-12 col-xs-12 " style="margin-bottom:5px;">
          
          <div class="container" style="margin-top:20px;" >
            <nav aria-label="breadcrumb" role="navigation">
              <ol class="breadcrumb">
                <li class="breadcrumb-item "><a style="color:#2e9141;" href="{{route('Accueil')}}">Accueil</a></li>
                <li aria-current="page" class="breadcrumb-item active">Informations</li>
                <li aria-current="page" class="breadcrumb-item active">Actualités</li>
              </ol>
            </nav>
            <h3 class="text-center">ACTUALITES</h3><hr> <br>
          </div>

          <div class="container latest-news pb-4" style="box-shadow: 2px 2px 2px #2e9141;">
          <div class="row">
            @foreach ($actualites as $an)  
            <div class="col-lg-6">
                      <div class="news-block" >
                        @if($an->image)
                        <div style="background: url('{{ Voyager::image( $an->image) }}'); height: 250px; " class="news-block-inner bg-image" class="img-fluid">
                        @else
                        <div style="background: url('frontend/news.jpg'); height: 250px;" class="news-block-inner bg-image">
                        @endif
                          <small class=""> Comptes rendus | <time>{{Carbon\Carbon::parse($an->created_at)->format('d M Y')}}</time> </small>
                          <h5 style="color: #ffffff;">{!!$an->titre!!}</h5>
                          <?php $titre2 = str_replace(" ", "-", $an->titre); ?>
                          <p></p><a  href="{{route('Actualitedetail',['id'=> $an->id, 'title'=>$titre2])}}" class="btn btn-outline-success">Lire plus</a>
                        </div>
                      </div>
                    </div>
             @endforeach
             </div>
            <div class="overlay d-flex align-items-center justify-content-center" style="text-size: 10px">
            {{$actualites->links('pagination::bootstrap-4')}}
              </div>
          </div>
            
        
      </section>
      <aside class="col-lg-4 col-md-4 col-sm-12 col-xs-12 fixed" style="margin-bottom:7px;">
      <section class="info-boxes pt-0 pb-0" style="margin-top:20px;">
<div class="card" style="background-color:#e9ecef;">
  <div class="card-header" style="background-color:color:#e9ecef;"><h5 style=" color:#777;">Catégories</h5></div>
  <div class="card-body">
    <div class="row">
      <div class="col-xs-12">
        <ul class="tags list-inline">
            @foreach ($cles as $cat)
            <li class="list-inline-item"><a href="{{route('Actualitecat',['id'=> $cat->id])}}" class="tag"><strong><?php echo "{$cat->nom } " ?></strong><strong></strong></a></li>
            @endforeach
        </ul>
      </div>
    </div>
  </div>
</div>
</section>

<br>
          <div class="card " style="background-color:#e9ecef;">
                    <div class="card-header text-center" style="background-color:#e9ecef;"><h5 style="color:#777;">Liens utiles</h5></div>
                    <div class="card-body" style="font-size: 13px;">
                      <div class="row">
                            <div class="px-3">
                              <p class="mt-1">
                                  <a target="_blank" href="#" class="text-black">
                                    SoNaMA en Bref
                                  </a><br>
                              </p>
                              <hr>
                              <p class="mt-3">
                                  <a target="_blank" href="#" class="text-black">
                                    Pré-Récolte 
                                  </a><br>
                              </p> <hr>
                              <p class="mt-3">
                                  <a target="_blank" href="#" class="text-black">
                                    Prestations
                                  </a><br>
                              </p> <hr>
                              
                              <p class="mt-3">
                                  <a target="_blank" href="#" class="text-black">
                                    Miriaa
                                  </a><br>
                              </p> <hr>
                              <p class="mt-3">
                              <a target="_blank" href="#" class="text-black">
                                     Nous Contacter
                                  </a>
                              </p> <hr>
                            </div>
                      
                    </div>
          </div>
          
      </aside>

  </div>

             
    </div>
@endsection