<footer id="footer">
        <!-- Widgets Start -->
        <div class="footer-widgets">
        <div class="container">
                <div class="row">
                    <div class="col-md-4 col-xl-4 d-flex justify-content-center">
                        <div class="single-widget contact-widget" data-aos="fade-up" data-aos-delay="0">
                            <h6 class="widget-tiltle">&nbsp;</h6>
                            <div class="media">
                                <i class="fa fa-map-marker"></i>
                                <div class="media-body ml-3">
                                    <h6>Addresse</h6>
                                    {{$description->adresse}}
                                </div>
                            </div>
                            <div class="media">
                                <i class="fa fa-envelope-o"></i>
                                <div class="media-body ml-3">
                                    <h6>Avez vous des questions?</h6>
                                    <a href="mailto:support@steelthemes.com">{{$description->email}}</a>
                                </div>
                            </div>
                            <div class="media">
                                <i class="fa fa-phone"></i>
                                <div class="media-body ml-3">
                                    <h6>Contactez nous</h6>
                                    <a href="tel:+22951209085">{{$description->contact}}</a>
                                </div>
                            </div>
                           
                        </div>
                    </div>
                    <div class="col-md-4 col-xl-4 d-flex justify-content-center">
                            <div class="single-widget tags-widget" data-aos="fade-up" data-aos-delay="800" style="text-align:center">
                                 <h6 class="widget-tiltle">&nbsp;</h6>
                                   <h5 class="widget-tiltle keys" >Mots clés</h5>
                                     @foreach ($cles as $cle)
                                      <a href="{{route('Actualitecat',['id'=> $cle->cle])}}" >{{$cle->nom}}</a>
                                     @endforeach
                            </div>
                    </div>
                        <div class="col-md-4 col-xl-4 d-flex justify-content-center">
                            <div class="single-widget single tags-widget" style="text-align:center" data-aos="fade-up" data-aos-delay="800">
                             <h6 class="widget-tiltle">&nbsp;</h6>
                              <h5 class="widget-tiltle">Réseaux sociaux</h5>
                                <ul class="nav social-nav">
                                    <li><a href="{{$description->facebook}}"  target="_blank"><i class="fa fa-facebook" ></i></a></li>
                                    <li><a href="{{$description->email}}" target="_blank"><i class="fa fa-google-plus"  ></i></a></li>
                                    <li><a href="#" target="_blank"><i class="fa fa-twitter" ></i></a></li>
                                    <li><a href="{{$description->linkdin}}" target="_blank"><i class="fa fa-linkedin" ></i></a></li>
                                </ul>
                      
                           </div>
                       </div>
                    </div>
           
        </div>
        </div>
        <!-- Widgets End -->
        <!-- Foot Note Start -->
        <div class="foot-note">
            <div class="container">
                <div
                    class="footer-content text-center  justify-content-between ">
                    <p >&copy; 2021 <a href="https://freehtml5.co/multipurpose" target="_blank" class="fh5-link">SoNaMA - BENIN</a></p>
                                   </div>
            </div>
        </div>
        <!-- Foot Note End -->
    </footer>