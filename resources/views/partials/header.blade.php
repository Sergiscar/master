
    <header class="position-absolute w-100">
        <div class="container">
            <div class="top-header d-none d-sm-flex justify-content-between align-items-center">
                <div class="contact">
                    <a href="tel:+1234567890" class="tel"><i class="fa fa-phone" aria-hidden="true"></i>(+229) 51 20 90 85</a>
                    <a href="mailto:contact@anama.bj"><i class="fa fa-envelope"
                            aria-hidden="true"></i>{{$description->email}}</a>
                </div>
                <nav class="d-flex aic">
                    <ul class="nav social d-none d-md-flex">
                        <li><a href="{{$description->facebook}}" target="_blank"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="{{$description->email}}"><i class="fa fa-youtube"></i></a></li>
                        <li><a href="{{$description->linkdin}}" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                    </ul>
                </nav>
            </div>
            <nav class="navbar navbar-expand-md navbar-light">
                <a class="navbar-brand" href="{{route('Accueil')}}"><img src="{{Voyager::image($description->logo)}}" alt="SoNaMA"></a>
               
              
               
                <div id="navbarSupportedContent" class=" collapse navbar-collapse">
                          <div class="navbar-nav" style="margin: auto;">
                            <div class="nav-item" ><a href="{{route('Accueil')}}"  class="vert nav-link  {{ (request()->routeIs('Accueil')) ? 'active' : '' }}">Accueil <span class="sr-only">(current)</span></a></div>
                            <div class="nav-item dropdown" ><a id="navbarDropdownMenuLink" data-hover="dropdown" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="jaune nav-link {{ (request()->routeIs(['Personnel','Societe'])) ? 'active' : '' }}">La société<i class="fa fa-angle-down"></i></a>
                              <ul aria-labelledby="navbarDropdownMenuLink" class="dropdown-menu">
                                <li><a href="{{route('Societe')}}"  class="dropdown-item nav-link text-success">Description</a></li>
                                <li><a href="{{route('Personnel')}}"  class="dropdown-item nav-link text-success">Personnel</a></li>
                                    
                              </ul>
                            </div>
                            <!-- multi-level dropdown-->
                            <div class="nav-item dropdown" ><a id="navbarDropdownMenuLink" data-hover="dropdown" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="rouge nav-link {{ (request()->routeIs(['Prerecolte','Postrecolte','Numerique','Prestation'])) ? 'active' : '' }}">SERVICES<i class="fa fa-angle-down"></i></a>
                              <ul aria-labelledby="navbarDropdownMenuLink" class="dropdown-menu">
                                <li><a href="{{route('Ingenierie')}}"  class="dropdown-item nav-link text-success">Ingénierie</a></li>
                                <li><a href="{{route('Production')}}"  class="dropdown-item nav-link text-success">Production</a></li>
                                <li><a href="{{route('Numerique')}}"  class="dropdown-item nav-link text-success">Numérique</a></li>
                                <li><a href="{{route('Commercial')}}"  class="dropdown-item nav-link text-success">Commercial</a></li> 
                                <li><a href="{{route('Sav')}}"  class="dropdown-item nav-link text-success">Service Après Vente</a></li> 

                              </ul>
                            </div>
                            <div class="nav-item dropdown"><a id="navbarDropdownMenuLink" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="vert nav-link {{ (request()->routeIs(['Actualite','Documentation','Newsletter'])) ? 'active' : '' }}">Informations<i class="fa fa-angle-down"></i></a>
                              <ul aria-labelledby="navbarDropdownMenuLink" class="dropdown-menu">
                                <li><a href="{{route('Actualite')}}"  class="dropdown-item nav-link text-success">Actualités</a></li>
                                <li><a href="{{route('Newsletter')}}"  class="dropdown-item nav-link text-success">Newsletter</a></li>
                                <li><a href="{{route('Documentation')}}"  class="dropdown-item nav-link text-success">Documentation</a></li>
                              </ul>
                            </div>
                            <div class="nav-item" ><a href="https://www.flickr.com/photos/190316110@N04/" class="jaune nav-link" target="_blank">Galerie</a></div>
                          </div>
                        </div>
            </nav>

           
        </div>
    </header>