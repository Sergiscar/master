@extends('layouts.app')

@section('title')
    <title>SoNaMA - BENIN</title>
@endsection

@section('style')
 
@endsection


@section('content')
<section class="hero hero-home" >
            <div class="swiper-container hero-slider">
              <div class="swiper-wrapper">
                @foreach ($carrousels as $carousel)
                <div class="swiper-slide " style="height:500px">
                <div style="background: url('{{Voyager::image($carousel->image)}}');height:600px;  background-size: cover;" class="hero-content slide-content d-flex align-items-center">
                    <div class="container "  >
                      <div class="row text">
                          <h1 data-aos="fade-right" data-aos-delay="200">Nos Actualités
                                    </h1>
                      </div>
                     </div>
                  </div>
                </div>
                @endforeach
        
              </div>
            
            </div>
            <span class="arr-left"><i class="fa fa-angle-left"></i></span>
            <span class="arr-right"><i class="fa fa-angle-right"></i></span>
</section>
<div class="main-container container" >
  <div class="row mt-0" style="margin-left:1px; margin-right:1px;  background-color:#ffffff;">
  <section class="col-lg-8 col-md-8 col-sm-12 col-xs-12 " style="margin-bottom:5px;">
          
          <div class="container" style="margin-top:20px;" >
            <nav aria-label="breadcrumb" role="navigation">
              <ol class="breadcrumb">
                <li class="breadcrumb-item "><a style="color:#2e9141;" href="{{route('Accueil')}}">Accueil</a></li>
                <li aria-current="page" class="breadcrumb-item active">Informations</li>
                <li aria-current="page" class="breadcrumb-item active">Actualités</li>
                <li aria-current="page" class="breadcrumb-item active">{{ $mavar }}</li>
              </ol>
            </nav>
            <h3 class="text-center">ACTUALITES</h3><hr> <br>
          </div>
          <div class="container pb-4" style="box-shadow: 2px 2px 2px #2e9141;">
          @if($actualites->count()==0)
          <div class=" blog-post" style="margin-top:5px;">
          Aucune actualité disponible pour cette catégorie.
          </div>
          @else
            @foreach ($actualites as $item)  
            <div class=" blog-post" style="margin-top:5px;">
                <div class="row">
                  <div class="col-md-4">
                      @if($item->image)
                      <div class=""><img src="{{Voyager::image($item->image)}}" alt="Actualité" class="img-fluid">
                      @else 
                      <div class=""><img src="{{asset('/frontend/news.jpg')}}" alt="" class="img-fluid">
                      @endif
                      </div>
                  </div>
                  <div class="col-md-8">
                    <div class=""> 
                    <?php $titre2 = str_replace(" ", "-", $item->titre); ?>
                      <a href="{{route('Actualitedetail',['id'=> $item->id,'title'=>$titre2])}}"><h5 class="text-this">{!!$item->titre!!}</h5></a>
                      <ul class="post-meta list-inline">
                        <li class="list-inline-item"><i class="fa fa-calendar"></i> <time>{{Carbon\Carbon::parse($item->created_at)->format('d M Y')}}</time> </li>
                        <li class="list-inline-item">
                      </ul>
                    </div>
                  </div>
                </div>
            </div>
            @endforeach
            @endif
            <div class="overlay d-flex align-items-center justify-content-center" style="text-size: 10px">
            {{$actualites->links('pagination::bootstrap-4')}}
              </div>
          </div>
            
        
      </section>
      <aside class="col-lg-4 col-md-4 col-sm-12 col-xs-12 fixed" style="margin-bottom:7px;">
      <section class="info-boxes pt-0 pb-0" style="margin-top:20px;">
<div class="card" style="background-color:#e9ecef;">
  <div class="card-header" style="background-color:color:#e9ecef;"><h5 style=" color:#777;">Catégories</h5></div>
  <div class="card-body">
    <div class="row">
      <div class="col-xs-12">
        <ul class="tags list-inline">
            @foreach ($cles as $cat)
            <li class="list-inline-item"><a href="{{route('Actualitecat',['id'=> $cat->id])}}" class="tag"><strong><?php echo "{$cat->nom } " ?></strong><strong></strong></a></li>
            @endforeach
        </ul>
      </div>
    </div>
  </div>
</div>
</section>

<br>
          <div class="card " style="background-color:#e9ecef;">
                    <div class="card-header text-center" style="background-color:#e9ecef;"><h5 style="color:#777;">Liens utiles</h5></div>
                    <div class="card-body" style="font-size: 13px;">
                      <div class="row">
                            <div class="px-3">
                              <p class="mt-1">
                                  <a target="_blank" href="#" class="text-black">
                                    SoNaMA en Bref
                                  </a><br>
                              </p>
                              <hr>
                              <p class="mt-3">
                                  <a target="_blank" href="#" class="text-black">
                                    Pré-Récolte 
                                  </a><br>
                              </p> <hr>
                              <p class="mt-3">
                                  <a target="_blank" href="#" class="text-black">
                                    Prestations
                                  </a><br>
                              </p> <hr>
                              
                              <p class="mt-3">
                                  <a target="_blank" href="#" class="text-black">
                                    Miriaa
                                  </a><br>
                              </p> <hr>
                              <p class="mt-3">
                              <a target="_blank" href="#" class="text-black">
                                     Nous Contacter
                                  </a>
                              </p> <hr>
                            </div>
                      
                    </div>
          </div>
          
      </aside>

  </div>

             
    </div>
@endsection