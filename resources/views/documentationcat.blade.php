@extends('layouts.front')

@section('title')
    <title>SoNaMA - BENIN</title>
@endsection

@section('style')
 
@endsection


@section('content')
<section class="hero hero-home" >
            <div class="swiper-container hero-slider">
              <div class="swiper-wrapper">
                @foreach ($carrousels as $carousel)
                <div class="swiper-slide " style="height:500px">
                <div style="background: url('{{Voyager::image($carousel->image)}}');height:600px;  background-size: cover;" class="hero-content slide-content d-flex align-items-center">
                      </div>
                </div>
                @endforeach
        
              </div>
            
            </div>
            <span class="arr-left"><i class="fa fa-angle-left"></i></span>
            <span class="arr-right"><i class="fa fa-angle-right"></i></span>
</section>
<div class="main-container container" >
  <div class="row mt-0" style="margin-left:1px; margin-right:1px;  background-color:#ffffff;">
  <section class="col-lg-8 col-md-8 col-sm-12 col-xs-12 " style="margin-bottom:5px;">
          
          <div class="container" style="margin-top:20px;" >
            <nav aria-label="breadcrumb" role="navigation">
              <ol class="breadcrumb">
                <li class="breadcrumb-item "><a style="color:#2e9141;" href="{{route('Accueil')}}">Accueil</a></li>
                <li aria-current="page" class="breadcrumb-item active">Informations</li>
                <li aria-current="page" class="breadcrumb-item active">Documentation</li>
              </ol>
            </nav>
            <h3 class="text-center">DOCUMENTATION</h3><hr> <br>
          </div>

          <section class="latest-news mt-2">
            <div class="container">
              <ul id="pills-tab" role="tablist" class="nav nav-pills nav-justified">
                <li class="nav-item"><a id="prospects-tab" data-toggle="pill" href="#pills-prospects" role="tab" aria-controls="pills-prospects" aria-selected="false" class="nav-link active">Pré-Récolte</a></li>
                <li class="nav-item"><a id="students-tab" data-toggle="pill" href="#pills-students" role="tab" aria-controls="pills-students" aria-selected="true" class="nav-link ">Post-Récolte</a></li>
                <li class="nav-item"><a id="teachers-tab" data-toggle="pill" href="#pills-teachers" role="tab" aria-controls="pills-teachers" aria-selected="false" class="nav-link">Numérique</a></li>
              </ul>
              <div id="pills-tabContent" class="tab-content">
                <div id="pills-students" role="tabpanel" aria-labelledby="students-tab" class="tab-pane fade ">                   
                  <div class="row mt-2">
                  @if($doc_post->count()==0)
          <div class=" blog-post" style="margin-top:5px;">
          Aucune nouvelle publication.
          </div>
          @else
                    @foreach ($doc_post as $ae)
                    <section class="Block Kiosk col-lg-4 col-md-4 " style="display: flex;-webkit-box-direction: reverse;flex-direction: row-reverse;-webkit-box-pack: center;justify-content: center ;color: #036; background-color: #f0f3f6;">
                      <div class="KioskContent" style="text-align:center;">
                        <p class="Intro">{!!$item->titre!!}</p>
                          <?php $file = (json_decode($item->file))[0]->download_link; ?>
                              <a href="{{Voyager::image($file)}}" target="_blank" title="{!!$item->titre!!}">
                                <figure class="KioskCover">
                                   <img class="block" src="{{Voyager::image($item->image)}}" width="200" alt="Couverture du Point N° 2567 du 21 octobre 2021" pinger-seen="true">
                                </figure>
                             </a>
                              <div>{!!$item->parution!!}</div>
        
                        <a class="btn btn-outline-success" href="{{Voyager::image($file)}}" target="_blank" class="Button Button--numeric">Lire</a>
  
                      </div>
   
                    </section>
                    @endforeach
                    @endif
                  </div>
                </div>
                <div id="pills-teachers" role="tabpanel" aria-labelledby="teachers-tab" class="tab-pane fade">
                  <div class="row mt-2">
                  @if($doc_num->count()==0)
          <div class=" blog-post" style="margin-top:5px;">
          Aucune nouvelle publication.
          </div>
          @else
                    @foreach ($doc_num as $item)
                         <section class="Block Kiosk col-lg-4 col-md-4 " style="display: flex;-webkit-box-direction: reverse;flex-direction: row-reverse;-webkit-box-pack: center;justify-content: center ;color: #036; background-color: #f0f3f6;">
                              <div class="KioskContent" style="text-align:center;">
      
                                  <p class="Intro">{!!$item->titre!!}</p>
                                      <?php $file = (json_decode($item->file))[0]->download_link; ?>
                                        <a href="{{Voyager::image($file)}}" target="_blank" title="{!!$item->titre!!}">
                                           <figure class="KioskCover">
                                              <img class="block" src="{{Voyager::image($item->image)}}" width="200" alt="Couverture du Point N° 2567 du 21 octobre 2021" pinger-seen="true">
                                           </figure>
                                        </a>
                                        <div>{!!$item->parution!!}</div>
        
                                       <a class="btn btn-outline-success" href="{{Voyager::image($file)}}" target="_blank" class="Button Button--numeric">Lire</a>
  
                              </div>
   
                         </section>
                    @endforeach
                    @endif
                
                  </div>
                </div>
                <div id="pills-prospects" role="tabpanel" aria-labelledby="prospects-tab" class="tab-pane fade show active">
                  <div class="row mt-2">
                  @if($doc_pre->count()==0)
          <div class=" blog-post" style="margin-top:5px;">
          Aucune nouvelle publication.
          </div>
          @else
                    @foreach ($doc_pre as $item)
                        <section class="Block Kiosk col-lg-4 col-md-4 " style="display: flex;-webkit-box-direction: reverse;flex-direction: row-reverse;-webkit-box-pack: center;justify-content: center ;color: #036; background-color: #f0f3f6;">
                            <div class="KioskContent" style="text-align:center;">
    
                              <p class="Intro">{!!$item->titre!!}</p>
                                  <?php $file = (json_decode($item->file))[0]->download_link; ?>
                                    <a href="{{Voyager::image($file)}}" target="_blank" title="{!!$item->titre!!}">
                                       <figure class="KioskCover">
                                          <img class="block" src="{{Voyager::image($item->image)}}" width="200" alt="Couverture du Point N° 2567 du 21 octobre 2021" pinger-seen="true">
                                       </figure>
                                    </a>
                                   <div>{!!$item->parution!!}</div>
        
                                   <a class="btn btn-outline-success" href="{{Voyager::image($file)}}" target="_blank" class="Button Button--numeric">Lire</a>
  
                            </div>
   
                        </section>     
                    @endforeach
                    @endif
                  </div>
                </div>
              </div>
            </div>
          </section>             
        
      </section>
      <aside class="col-lg-4 col-md-4 col-sm-12 col-xs-12 fixed" style="margin-bottom:7px;">
      <section class="info-boxes pt-0 pb-0" style="margin-top:20px;">
<div class="card" style="background-color:#e9ecef;">
  <div class="card-header" style="background-color:color:#e9ecef;"><h5 style=" color:#777;">Catégories</h5></div>
  <div class="card-body">
    <div class="row">
      <div class="col-xs-12">
        <ul class="tags list-inline">
            @foreach ($cles as $cat)
            <li class="list-inline-item"><a href="{{route('Actualitecat',['id'=> $cat->cle])}}" class="tag"><strong><?php echo "{$cat->nom } " ?></strong><strong></strong></a></li>
            @endforeach
        </ul>
      </div>
    </div>
  </div>
</div>
</section>

<br>
          <div class="card " style="background-color:#e9ecef;">
                    <div class="card-header text-center" style="background-color:#e9ecef;"><h5 style="color:#777;">Liens utiles</h5></div>
                    <div class="card-body" style="font-size: 13px;">
                      <div class="row">
                            <div class="px-3">
                              <p class="mt-1">
                                  <a target="_blank" href="#" class="text-black">
                                    SoNaMA en Bref
                                  </a><br>
                              </p>
                              <hr>
                              <p class="mt-3">
                                  <a target="_blank" href="#" class="text-black">
                                    Pré-Récolte 
                                  </a><br>
                              </p> <hr>
                              <p class="mt-3">
                                  <a target="_blank" href="#" class="text-black">
                                    Prestations
                                  </a><br>
                              </p> <hr>
                              
                              <p class="mt-3">
                                  <a target="_blank" href="#" class="text-black">
                                    Miriaa
                                  </a><br>
                              </p> <hr>
                              <p class="mt-3">
                              <a target="_blank" href="#" class="text-black">
                                     Nous Contacter
                                  </a>
                              </p> <hr>
                            </div>
                      
                    </div>
          </div>
          
      </aside>

  </div>

             
    </div>
@endsection