@extends('layouts.front')

@section('title')
    <title> SoNaMA - BENIN</title>
@endsection

@section('style')

@endsection


@section('content')
<section class="hero hero-home" >
            <div class="swiper-container hero-slider">
              <div class="swiper-wrapper">
                @foreach ($carrousels as $carousel)
                <div class="swiper-slide " style="height:500px">
                <div style="background: url('{{Voyager::image($carousel->image)}}');height:600px;  background-size: cover; justify-content: center; margin-top :-10px;" class="hero-content slide-content d-flex align-items-center">
                  </div>
                </div>
                @endforeach
        
              </div>
            
            </div>
          
</section>
<div class="main-container container" >
  <div class="row mt-0" style="margin-left:1px; margin-right:1px;  background-color:#ffffff;">
  <section class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="margin-bottom:10px;">
          
          <div class="container" style="margin-top:20px;" >
            <nav aria-label="breadcrumb" role="navigation">
              <ol class="breadcrumb">
                <li class="breadcrumb-item "><a style="color:#2e9141;" href="{{route('Accueil')}}">Accueil</a></li>
                <li aria-current="page" class="breadcrumb-item active">SoNaMA</li>
                <li aria-current="page" class="breadcrumb-item active">Description</li>
              </ol>
            </nav>
            <div class="container" style="  box-shadow: 2px 2px 2px #2e9141;">

                    
                
                  <div class="row mt-5">
                    <div class="col-md-5 col-lg-5" style="justify-content: center; padding-top: 90px;"  >
                       <img src="{{ Voyager::image( $post->image) }}"  alt="employes" class="img-fluid " >
                    </div>
                    <div class="col-md-7 col-lg-7 text-content" > 
                      <h2><?php  echo "{$post->titre  }"?> </h2>
                          <?php  echo "{$post->partie_un }"?> 
                    </div>
                    <div class="col-md-12  col-lg-12 text-content " style="margin-top:-60px">
                      <?php  echo "{$post->partie_deux }"?> 
                    </div>
                  </div>
                  <h2 style="text-align:center; margin-top: 20px;"><?php  echo "Le Conseil d'administration"?> </h2>
                  <div class="col-md-12  col-lg-12 text-content " >
                      <?php  echo "{$post->conseil_d_administration }"?> 
                    </div>
                    <h2 style="text-align:center ; margin: 40px 0px 30px;"><?php  echo "L'Organigramme"?> </h2>
                  <div class="col-md-12  col-lg-12 text-content " >
                    <?php  echo "{$post->description_organigramme  }"?>
                  <img src="{{ Voyager::image( $post->fichier_organigramme) }}"  alt="employes" class="img-fluid " >
                    </div>
              </div>
           
              </div>
             </div> 
      </section>

  
  </div>      
    </div>
    @endsection