@extends('layouts.admin')

@section('title')
    <title>Newsletter</title>
@endsection

@section('style')
<style>
    body {
    color: #000;
    overflow-x: hidden;
    height: 100%;
    background-image: url("https://i.imgur.com/GMmCQHC.png");
    background-repeat: no-repeat;
    background-size: 100% 100%
}

.card {
    padding: 30px 40px;
    margin-top: 60px;
    margin-bottom: 60px;
    border: none !important;
    box-shadow: 0 6px 12px 0 rgba(0, 0, 0, 0.2)
}

.blue-text {
    color: #00BCD4
}

.form-control-label {
    margin-bottom: 0
}

input,
textarea,
button {
    padding: 8px 15px;
    border-radius: 5px !important;
    margin: 5px 0px;
    box-sizing: border-box;
    border: 1px solid #ccc;
    font-size: 18px !important;
    font-weight: 300
}

input:focus,
textarea:focus {
    -moz-box-shadow: none !important;
    -webkit-box-shadow: none !important;
    box-shadow: none !important;
    border: 1px solid #00BCD4;
    outline-width: 0;
    font-weight: 400
}

.btn-block {
    text-transform: uppercase;
    font-size: 15px !important;
    font-weight: 400;
    height: 43px;
    cursor: pointer
}

.btn-block:hover {
    color: #fff !important
}

button:focus {
    -moz-box-shadow: none !important;
    -webkit-box-shadow: none !important;
    box-shadow: none !important;
    outline-width: 0
}
</style>
@endsection


@section('content')
<div class="container-fluid px-1 py-5 mx-auto">
    <div class="row d-flex justify-content-center">
        <div class="col-xl-7 col-lg-8 col-md-9 col-11 text-center">
            <h3>Newsletter</h3>
       
            <div class="card">
                <h5 class=" blue-text text-center mb-4">Ce formulaire vous permet<br> de publier une newsletter et l'envoyer à tous les souscripteurs. </h5>
                <form class="form-card" action="{{route('voyager.Envoyernewsletter')}}" method="post" enctype="multipart/form-data"  >
                {{csrf_field()}}
                    <div class="row justify-content-between text-left">
                        <div class="form-group col-sm-12 flex-column d-flex"> <label class="form-control-label px-3">Titre<span class="text-danger"> </span></label> <input type="text" id="titre" name="titre" placeholder="" > </div>
                       
                    </div>
                    <div class="row justify-content-between text-left">
                        <div class="form-group col-sm-6 flex-column d-flex"> <label class="form-control-label px-3">Parution<span class="text-danger"> </span></label> <input type="text" id="parution" name="parution" placeholder="" onblur="validate(3)"> </div>
                        <div class="form-group col-sm-6 flex-column d-flex"> <label class="form-control-label px-3">Courte description<span class="text-danger"></span></label> <textarea type="text" style="font-size:24px;" id="courte_description" name="courte_description" placeholder="" > </textarea> </div>
                    </div>
                    <div class="row justify-content-between text-left">
                        
                        <div class="form-group col-sm-6 flex-column d-flex"> <label class="form-control-label px-3">Image<span class="text-danger"></span></label> <input type="file" id="image" name="image" accept="image/png, image/jpeg"> </div>
                        <div class="form-group col-sm-6 flex-column d-flex"> <label class="form-control-label px-3">Fichier<span class="text-danger"></span></label> <input type="file" id="file" name="file" > </div>
                    </div>
                    <div class="row justify-content-end">
                        <div class="form-group col-sm-6"> <button type="submit" class="btn-block btn-success">Envoyer</button> </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection