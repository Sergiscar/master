<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\IndexController;
use App\Http\Controllers\ActualiteController;
use App\Http\Controllers\ServiceController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//Route::get('/', ['as' => 'Accueil','uses' => 'IndexController@index']);
Route::get('/', [IndexController::class, 'index'])->name('Accueil');
Route::get('/actualites', [ActualiteController::class, 'index'])->name('Actualite');
Route::get('/actualite/art{id}/{title}',  [ServiceController::class, 'index'])->name('Actualitedetail');
Route::get('/service/{id}',  [ServiceController::class, 'service'])->name('Servicedetail');
Route::resource('sonama',  IndexController::class);
Route::group(['prefix' => 'admin'], function () {Voyager::routes();});

Route::get('/societe', [ActualiteController::class, 'societe'])->name('Societe');
Route::get('/ingenierie', [ServiceController::class, 'serviceing'])->name('Ingenierie');
Route::get('/production', [ServiceController::class, 'servicepro'])->name('Production');
Route::get('/numerique', [ServiceController::class, 'servicenum'])->name('Numerique');
Route::get('/commercial', [ServiceController::class, 'servicecom'])->name('Commercial');
Route::get('/sav', [ServiceController::class, 'servicesav'])->name('Sav');
Route::get('/prestation', [ServiceController::class, 'serviceprest'])->name('Prestation');
Route::get('/solution/bj{id}--/{titre}', [ServiceController::class, 'indexsolution'])->name('Solution');
Route::get('/actualitecat/{id}', [ActualiteController::class, 'actualitecat'])->name('Actualitecat');
Route::get('/documentation', [ActualiteController::class, 'documentation'])->name('Documentation');
Route::get('/newsletters', [ActualiteController::class, 'newsletter'])->name('Newsletter');
Route::get('/personnel', [ActualiteController::class, 'indexadministration'])->name('Personnel');
Route::post('/addmail', [IndexController::class, 'store'])->name('Addmail');
Route::post('/demande/prestation/sav', [ServiceController::class, 'saveDemandeSav'])->name('Demandeprestationsav');
Route::post('/demande/prestation/ssi', [ServiceController::class, 'saveDemandeSsi'])->name('Demandeprestationssi');
Route::post('/demande/prestation/scm', [ServiceController::class, 'saveDemandeScm'])->name('Demandeprestationscm');
Route::post('/demande/prestation/ing', [ServiceController::class, 'saveDemandeIng'])->name('Demandeprestationing');
Route::post('/demande/prestation/pro', [ServiceController::class, 'saveDemandePro'])->name('Demandeprestationpro');
Route::get('/ajax/prestation', [ServiceController::class, 'ajaxGetPrestation'])->name('ajaxGetPrestation');
